PAGEDJS_BINARY_PATH = 'pagedjs-cli' # os.path.join(os.path.dirname(__file__), 'node_modules/pagedjs-cli/bin/paged')
# Path to the pagedjs-cli executable
# Find it on linux with `whereis pagedjs-cli`
# Let's try the simple way first :-)
DEBUG = False 
# When set to true, the application doesn't generate a PDF, 
# but uses the pagedjs-polyfill to show a preview of the document
SITEURL = 'http://localhost:5000'
# Baseurl on the server. For now only in devployment
HTML_TMP_DIR = '/tmp/publishing_house/'
# Location where the temporary html files are stored
